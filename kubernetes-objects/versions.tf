# Terraform Settings Block
terraform {
  required_version = ">= 1.0.0"
  backend "s3" {
    bucket = "ayodele-terraform-eks"
    key    = "dev/k8s-objects.tfstate"
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      #version = "~> 3.70"
      version = "~> 4.60"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      #version = "~> 2.7"
      version = ">= 2.20"
    }
  }
}